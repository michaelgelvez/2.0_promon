<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Auth'], function (): void {
    Route::get('/', 'LoginController@showLoginForm');
    Route::get('/login', 'LoginController@showLoginForm')->name('login');
    Route::post('/login', 'LoginController@login')->name('login.post');
    Route::get('/logout', 'LoginController@logout')->name('logout');
    Route::get('/password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('/password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset')->name('password.update');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard/{id?}', 'DashboardController@index')->name('dashboard');
    Route::post('/dashboard/{id?}', 'DashboardController@index')->name('dashboard.post');
    Route::get('/analytics/{id?}', 'AnalyticsController@index')->name('analytics');
    Route::post('/analytics/{id?}', 'AnalyticsController@index')->name('analytics.post');
    Route::get('/applicants/{id?}', 'ApplicantController@index')->name('applicants');
    Route::post('/applicants/{id?}', 'ApplicantController@index')->name('applicants.post');
    Route::get('applicants/export/{id?}', 'ApplicantController@export')->name('applicants.export');

    Route::group(['prefix' => 'pages', 'as' => 'pages.'], function () {
        Route::get('/', 'PageController@index')->name('index');
        Route::get('/create', 'PageController@create')->name('create');
        Route::post('/save/{id?}', 'PageController@save')->name('save');
        Route::get('/edit/{id}', 'PageController@edit')->name('edit');
        Route::post('/delete', 'PageController@delete')->name('delete');
    });

    Route::group(['prefix' => 'forms', 'as' => 'forms.'], function () {
        Route::get('/', 'FormController@index')->name('index');
        Route::get('/create', 'FormController@create')->name('create');
        Route::post('/save/{id?}', 'FormController@save')->name('save');
        Route::get('/edit/{id}', 'FormController@edit')->name('edit');
        Route::post('/delete', 'FormController@delete')->name('delete');
    });

    Route::group(['prefix' => 'design', 'as' => 'design.'], function () {
        Route::get('/', 'MasterCssController@index')->name('index');
        Route::post('/', 'MasterCssController@save')->name('save');
    });

    Route::group(['prefix' => 'members', 'as' => 'members.'], function () {
        Route::get('/', 'MemberController@index')->name('index');
        Route::get('/create', 'MemberController@create')->name('create');
        Route::post('/save/{id?}', 'MemberController@save')->name('save');
        Route::get('/edit/{id}', 'MemberController@edit')->name('edit');
        Route::post('/delete', 'MemberController@delete')->name('delete');
        Route::get('/export', 'MemberController@export')->name('export');
    });

    Route::group(['prefix' => 'settings', 'as' => 'settings.'], function () {
        Route::get('/', 'ConfigController@index')->name('index');
        Route::post('/campaign', 'CampaignController@store')->name('campaign.store');
        Route::post('/fields', 'MasterFieldsController@save')->name('fields.save');
        Route::post('/user', 'UserFunctionalityController@save')->name('user.save');
        Route::post('/user/import', 'UserFunctionalityController@import')->name('user.import');
        Route::post('/security', 'SecurityController@save')->name('security.save');
    });
});

Route::get('/design/css', 'PromonFormController@showCSS')->name('dynamicForm.showCSS');
Route::get('/{permalink}', 'PromonFormController@show')->name('dynamicForm.show');
Route::post('/{permalink}', 'PromonFormController@save')->name('dynamicForm.save');
