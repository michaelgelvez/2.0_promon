<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterCss extends Model
{
    protected $table = 'master_css';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'content',
    ];

    /**
     * The storage format of the model's date columns.
     *
     * @var array
     */
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at',
    ];
}
