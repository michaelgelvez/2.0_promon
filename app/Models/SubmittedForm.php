<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubmittedForm extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'form_id',
    ];

    /**
     * The storage format of the model's date columns.
     *
     * @var array
     */
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at',
    ];

    /**
     * Scope a query to order by created at.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSubmittedForms($query)
    {
        return $query->orderBy('created_at', 'asc');
    }

    /**
     * The MasterInput that belong to the Question_Form.
     */
    public function form()
    {
        return $this->belongsTo(Form::class, 'form_id');
    }

    /**
     * Get the questions from the form.
     */
    public function answers()
    {
        return $this->hasMany(QuestionFormAnswer::class);
    }
}
