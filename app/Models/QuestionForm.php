<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionForm extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'form_id',
        'master_input_id',
        'type',
        'question',
        'before_text',
        'after_text',
        'is_required',
        'has_others',
    ];

    /**
     * The storage format of the model's date columns.
     *
     * @var array
     */
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at',
    ];

    /**
     * The Form that belong to the Question_Form.
     */
    public function form()
    {
        return $this->belongsTo(Form::class, 'form_id');
    }

    /**
     * The MasterInput that belong to the Question_Form.
     */
    public function MasterInput()
    {
        return $this->belongsTo(MasterInput::class, 'master_input_id');
    }

    /**
     * Get the answer from the question.
     */
    public function answers()
    {
        return $this->hasOne(QuestionFormAnswer::class);
    }
}
