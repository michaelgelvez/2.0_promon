<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterInput extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'name',
        'label',
        'options',
        'is_required',
    ];

    /**
     * The storage format of the model's date columns.
     *
     * @var array
     */
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'deleted_at',
    ];

    /**
     * The Master Input that belong to the Question_Form.
     */
    public function questions()
    {
        return $this->hasOne(QuestionForm::class);
    }
}
