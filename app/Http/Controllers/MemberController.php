<?php

namespace App\Http\Controllers;

use App\Exports\MembersExport;
use App\Models\Member;
use App\Models\MemberHeader;
use App\Services\MembersService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Member       $member
     * @param MemberHeader $memberField
     * @return \Illuminate\Http\Response
     */
    public function index(Member $member, MemberHeader $memberField)
    {
        return view('members.index')
            ->with([
                'members' => $member,
                'memberField' => $memberField,
            ]);
    }

    /**
     * Go to create member form.
     *
     * @param Member         $member
     * @param MembersService $service
     * @return \Illuminate\Http\Response
     */
    public function create(MembersService $service)
    {
        $content = $service->generateContent();

        return view('members.create')
            ->with('content', $content);
    }

    /**
     * Save registered member in database.
     *
     * @param Member         $member
     * @param Request        $request
     * @param MembersService $service
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request, MembersService $service)
    {
        if (!$request->route('id')) {
            $service->create($request->except('_token'));
        } else {
            $service->update(array_merge($request->except('_token'), ['id' => $request->route('id')]));
        }

        return redirect()->route('members.index');
    }

    /**
     * Get the details of specified member in database.
     *
     * @param $id
     * @param Member         $members
     * @param MembersService $service
     * @return \Illuminate\Http\Response
     */
    public function edit($id, MembersService $service)
    {
        $content = $service->generateContent($id);

        return view('members.create')
            ->with('content', $content);
    }

    /**
     * Bulk delete members.
     *
     * @param Request     $request
     * @param NewsService $service
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        Member::whereIn('id', $request->id)->delete();

        return redirect()->route('members.index');
    }

    /**
     * Export the data from database.
     *
     * @return \Illuminate\Http\Response
     */
    public function export()
    {
        return Excel::download(new MembersExport, 'members' . date('Ymd_H.i.s') . '.csv');
    }
}
