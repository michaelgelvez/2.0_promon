<?php

namespace App\Http\Controllers;

use App\Http\Requests\MasterCssRequest;
use App\Models\MasterCss;
use Illuminate\Http\Request;
use Session;

class MasterCssController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $masterCss = MasterCss::find(1);

        return view('design')
            ->with('masterCss', $masterCss);
    }

    /**
     * Save created content in database.
     *
     * @param \Illuminate\Http\Request $request
     * @param MasterCss                $masterCss
     * @return \Illuminate\Http\Response
     */
    public function save(MasterCss $masterCss, MasterCssRequest $request)
    {
        $masterCss->find(1)->fill($request->except('_token'))->save();
        Session::flash('success_design', '更新しました');

        return redirect()->route('design.index');
    }
}
