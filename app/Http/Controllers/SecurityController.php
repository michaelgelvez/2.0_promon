<?php

namespace App\Http\Controllers;

use App\Http\Requests\SecurityRequest;
use App\Models\RestrictAddress;
use Illuminate\Http\Request;
use Session;

class SecurityController extends Controller
{
    /**
     * Save created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function save(SecurityRequest $request)
    {
        //incoming data
        $incoming = collect($request->ip)->map(function ($item) {
            return $item;
        });

        //existing data
        $existing = collect(RestrictAddress::all())->map(function ($item) {
            return  $item->ip;
        });

        $insertedData = $incoming->diff($existing);
        $deletedData = $existing->diff($incoming);

        // delete ip addresses
        collect($deletedData)->each(function ($item) {
            RestrictAddress::where('ip', $item)->delete();
        });

        // insert or update new ip addresses
        RestrictAddress::insert(
            collect($insertedData)->map(function ($item) {
                return [
                    'ip' => $item,
                ];
            })->all()
        );

        Session::flash('success_security', '更新しました');

        return redirect()->to(route('settings.index') . '#security');
    }
}
