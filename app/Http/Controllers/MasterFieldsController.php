<?php

namespace App\Http\Controllers;

use App\Http\Requests\MasterFieldsRequest;
use App\Models\MasterInput;
use Carbon\Carbon;
use Session;

class MasterFieldsController extends Controller
{
    /**
     * Save created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function save(MasterFieldsRequest $request)
    {
        //incoming data
        $incoming = collect($request->except('_token'))->map(function ($item) {
            return  json_encode([
                'type' => $item['type'],
                'name' => $item['name'],
                'label' => in_array($item['type'], ['radio', 'checkbox', 'select']) ? null : $item['label'],
                'options' => in_array($item['type'], ['radio', 'checkbox', 'select'])
                                ? json_encode($item['options'])
                                : null,
            ]);
        });

        //existing data
        $existing = collect(MasterInput::all())->map(function ($item) {
            return  json_encode([
                'type' => $item['type'],
                'name' => $item['name'],
                'label' => $item['label'],
                'options' => in_array($item['type'], ['radio', 'checkbox', 'select'])
                                ? json_encode(json_decode($item['options']))
                                : null,
            ]);
        });

        $insertedData = $incoming->diff($existing);
        $deletedData = $existing->diff($incoming);

        // delete master fields
        collect($deletedData)->each(function ($item) {
            $item = json_decode($item);
            MasterInput::where('name', $item->name)->where('type', $item->type)->delete();
        });

        //re-insert master fields
        MasterInput::insert(
            collect($insertedData)->map(function ($item) {
                $item = json_decode($item);
                return [
                    'type' => $item->type,
                    'name' => $item->name,
                    'label' => $item->label,
                    'options' => in_array($item->type, ['radio', 'checkbox', 'select'])
                                    ? $item->options
                                    : null,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
            })->all()
        );

        Session::flash('success_masterField', '更新しました');

        return redirect()->to(route('settings.index') . '#fieldmaster');
    }
}
