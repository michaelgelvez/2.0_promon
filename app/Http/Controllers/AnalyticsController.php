<?php

namespace App\Http\Controllers;

use App\Models\Form;
use App\Models\SubmittedForm;

class AnalyticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param null|mixed $id
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        $forms = Form::all();
        $submittedForms = SubmittedForm::with(['form'])->SubmittedForms()->where('form_id', $id)->get();

        $submittedFormGroupedDates = $submittedForms->groupBy(function ($data) {
            return $data->created_at->format('m月d日');
        });

        $labels = $submittedFormGroupedDates->keys();

        $data = $submittedFormGroupedDates->map(function ($value) {
            return $value->count();
        })->values();

        $analytics = [
            'labels' => $labels,
            'submittedForms' => $submittedForms->count(),
            'allSubmittedForms' => SubmittedForm::all()->count(),
        ];

        $datasets = collect([
            'label' => $submittedForms->first()->form->title ?? '',
            'borderColor' => 'rgba(39, 117, 246, 1)',
            'data' => $data,
        ]);

        return view('analytics')->with([
            'forms' => $forms,
            'analytics' => $analytics,
            'datasets' => [$datasets],
        ]);
    }
}
