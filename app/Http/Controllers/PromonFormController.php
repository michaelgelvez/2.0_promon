<?php

namespace App\Http\Controllers;

use App\Mail\AdminNotification;
use App\Mail\UserNotification;
use App\Models\Campaign;
use App\Models\Form;
use App\Models\MasterCss;
use App\Models\MasterInput;
use App\Models\Page;
use App\Models\QuestionForm;
use App\Models\QuestionFormAnswer;
use App\Models\RestrictAddress;
use App\Models\SubmittedForm;
use App\Services\FormService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;

class PromonFormController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param \App\QuestionForm $questionForm
     * @param mixed             $permalink
     * @param Request           $request
     * @param FormService       $formService
     * @return \Illuminate\Http\Response
     */
    public function show($permalink, Request $request, FormService $formService)
    {
        $page = Page::where('permalink', $permalink)->first() ?? abort(404);
        $security = RestrictAddress::all()->pluck('ip')->contains($request->getClientIp());

        ($page->is_published == 0 || $security == true) ? abort(401) : '';

        $formCodes = preg_match_all('~[[]contact-form-id=(.*?)]~', $page->content, $formContents);

        $page->content = ($formCodes > 0) ? $formService->generateFormCode($page, $formContents[0]) : $page->content;

        return view('form')->with(
            [
                'page' => $page,
            ]
        );
    }

    /**
     * Save a newly created resource in storage.
     *
     * @todo Send Email, Validations, Others Field
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $data = collect($request->except('_token', 'id'))->values();
        $form = Form::with(['questions', 'questions.masterinput', 'config'])->findOrFail($request->id);

        $rules = collect($data)->mapWithKeys(function ($value, $key) use ($form) {
            $questionForm = QuestionForm::findOrFail($form->questions[$key]->id);
            $masterInput = MasterInput::findOrFail($questionForm->master_input_id);

            $isRequired = $questionForm->is_required == 1 ? 'required' : 'nullable';

            switch ($masterInput->type) {
                case 'radio':
                    $val = (isset($value['value']) && $value['value'] == 'other') ? 'other' : 'value';
                    $validation = $masterInput->name . '.' . $val;
                break;
                case 'checkbox':
                    $val = (isset($value['value']) && in_array('other', $value['value']) == true) ? 'other' : 'value';
                    $validation = $masterInput->name . '.' . $val;
                break;
                default:
                    $validation = $masterInput->name;
            }

            return [
                $validation => $isRequired,
            ];
        })->all();

        $request->validate($rules);

        $submittedForm = SubmittedForm::create(
            [
                'form_id' => $form->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                ]
            );

        QuestionFormAnswer::insert(
            collect($data)->map(function ($answer, $key) use ($form, $submittedForm) {
                if (is_array($answer)) {
                    if (collect($answer)->has('other')) {
                        $other = $answer['other'];
                        // Set 'other' based on other field
                        $answer = ($answer['value'] == 'other') ? $other : $answer['value'];

                        $answer = is_array($answer) ? implode(' ,', collect($answer)->diff('other')->merge($other)->toArray()) : $answer;
                    }
                }

                return [
                    'submitted_form_id' => $submittedForm->id,
                    'question_form_id' => $form->questions[$key]->id,
                    'answer' => $answer,
                    'other' => $other ?? null,
                ];
            })->all()
        );

        $redirectAfter = Campaign::find(1)->finish_redirect_url;

        if ($form->config->admin_is_notify == 1) {
            $emails = json_decode($form->config->admin_emails);
            $adminSubject = $form->config->admin_subject;

            foreach ($emails as $email) {
                Mail::to($email)->send(new AdminNotification($adminSubject, ''));
            }
        }

        if ($form->config->user_is_notify == 1) {
            $emails = json_decode($form->config->user_emails);
            $userSubject = $form->config->user_subject;
            $userContent = $form->config->user_email_content;

            foreach ($emails as $email) {
                Mail::to($email)->send(new UserNotification($userSubject, $userContent));
            }
        }

        return $redirectAfter === null ? back() : redirect($redirectAfter);
    }

    /**
     * Display Dynamic CSS on the specified resource.
     *
     * @param \App\QuestionForm $questionForm
     * @param mixed             $permalink
     * @return \Illuminate\Http\Response
     */
    public function showCSS()
    {
        $response = Response::make(MasterCss::find(1)->content);
        $response->header('Content-Type', 'text/css');
        return $response;
    }
}
