<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FormCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $questions = collect($request['questions'])->map(function ($value, $key) use ($request) {
            return [
                'id' => explode('-', $key, 2)[1],
                'question' => $request['questions'][$key]['question'],
                'is_required' => isset($request['questions'][$key]['is_required']) ? '1' : '',
                'has_others' => isset($request['questions'][$key]['has_others']) ? '1' : '',
                'type' => $request['questions'][$key]['type'] ?? '',
                'master_input_id' => $request['questions'][$key]['master_input_id'] ?? '',
                'before_text' => $request['questions'][$key]['before_text'] ?? '',
                'after_text' => $request['questions'][$key]['after_text'] ?? '',
            ];
        });

        return [
            'id' => '',
            'title' => $request['title'],
            'content' => '',
            'questions' => $questions,
            'config' => collect([
                    'admin_is_notify' => $request['admin_is_notify'],
                    'admin_subject' => $request['admin_subject'],
                    'admin_emails' => json_encode($request['admin_emails']),
                    'user_is_notify' => $request['user_is_notify'],
                    'user_subject' => $request['user_subject'],
                    'user_emails' => json_encode($request['user_emails']),
                    'user_email_content' => $request['user_email_content'], ]),
        ];
    }
}
