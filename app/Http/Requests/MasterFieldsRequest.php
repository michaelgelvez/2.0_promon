<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class MasterFieldsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validations = collect($this->except('_token'))->mapWithKeys(function ($item, $key) {
            return [
                ($key . '.type') => 'required',
                ($key . '.name') => 'required|regex:/^\S*$/u',
                ($key . '.options.*') => 'required_if:' . $key . '.type,radio,checkbox,select',
            ];
        })->toArray();
        $validations['*.name'] = 'distinct:ignore_case';

        return $validations;
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.name.required' => 'マスタフィールド名を入力して ください',
            '*.options.*' => '選択肢を入力してください',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        throw (new ValidationException($validator))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl() . '#fieldmaster');
    }
}
