<?php

namespace App\Exports;

use App\Models\Member;
use App\Models\MemberHeader;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class MembersExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
     * Get all members in database.
     *
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Member::all();
    }

    /**
     * Set the header for csv file.
     */
    public function headings(): array
    {
        $columns = MemberHeader::all();

        return $columns->pluck('field')->toArray();
    }
}
