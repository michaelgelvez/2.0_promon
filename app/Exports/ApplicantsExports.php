<?php

namespace App\Exports;

use App\Models\QuestionForm;
use App\Services\ApplicantService;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ApplicantsExports implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
     * Get all members in database.
     *
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $applicantService = new ApplicantService;
        $data = $applicantService->getApplicants(request('id'));

        return $data['answers'];
    }

    /**
     * Set the header for csv file.
     */
    public function headings(): array
    {
        $questionForm = QuestionForm::with(['answers'])->where('form_id', request('id'))->get();
        $questions = $questionForm->pluck('question')->toArray();

        return $questions;
    }
}
