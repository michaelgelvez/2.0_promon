<?php

namespace App\Services;

use App\Models\Form;
use App\Models\QuestionForm;
use App\Models\SubmittedForm;
use App\Utils\FormUtil;

class ApplicantService
{
    use FormUtil;

    /**
     * Get all applicants in database.
     *
     * @param $id
     */
    public function getApplicants($id)
    {
        $forms = Form::all();
        $submittedForm = SubmittedForm::with(['answers', 'answers.questions', 'answers.questions.MasterInput'])->where('form_id', $id)->get();
        $questionForm = QuestionForm::with(['answers'])->where('form_id', $id)->get();
        $questions = $questionForm->pluck('question');

        $answers = $submittedForm->map(function ($value, $key) use ($submittedForm) {
            $answer = $submittedForm[$key]->answers->pluck('answer');

            // get selection fields from the inputs
            $inputTypes = $submittedForm[$key]->answers;
            $selectionFields = $inputTypes->pluck('questions.type')->filter(function ($value) {
                return $value == 'select';
            });

            if ($selectionFields->isNotEmpty()) {
                // get [ key => option value]
                $selectedItems = $selectionFields->keys()->map(function ($value) use ($answer, $inputTypes) {
                    return [$value => json_decode($inputTypes[$value]->questions->MasterInput->options)[$answer[$value]]];
                });

                // compare and replace array if matched with selected Items
                foreach ($selectedItems as $key => $item) {
                    $answer = collect(array_replace_recursive($answer->toArray(), $item));
                }
            }

            return is_array($answer) ? implode(', ', $answer) : $answer;
        });

        return [
            'forms' => $forms,
            'questions' => $questions,
            'answers' => $answers,
        ];
    }
}
