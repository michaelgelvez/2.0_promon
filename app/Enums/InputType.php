<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

final class InputType extends Enum implements LocalizedEnum
{
    // const BUTTON = 'button';

    const CHECKBOX = 'checkbox';

    // const COLOR = 'color';

    // const DATE = 'date';

    // const DATETIME_LOCAL = 'datetime-local';

    const EMAIL = 'email';

    // const FILE = 'file';

    // const HIDDEN = 'hidden';

    // const IMAGE = 'image';

    // const MONTH = 'month';

    // const NUMBER = 'number';

    // const PASSWORD = 'password';

    const RADIO = 'radio';

    // const RANGE = 'range';

    // const RESET = 'reset';

    // const SEARCH = 'search';

    // const SUBMIT = 'submit';

    // const TEL = 'tel';

    const TEXT = 'text';

    // const TIME = 'time';

    // const URL = 'url';

    // const WEEK = 'week';

    const SELECT = 'select';

    const TEXTAREA = 'textarea';
}
