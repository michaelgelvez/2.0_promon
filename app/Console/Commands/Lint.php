<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Lint extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lint:fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute PHP lint';

    /**
     * The lint commands.
     *
     * @var string
     */
    protected $commands = [
        ['vendor', 'bin', 'php-cs-fixer fix -v'],
        ['vendor', 'bin', 'phpcs --ignore=design --extensions=php .'],
        ['vendor', 'bin', 'phpmd . text ruleset.xml --suffixes php --exclude resources,storage,vendor,design,public'],
    ];

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $returnCode = collect($this->commands)->contains(function ($command) {
            return $this->exec(implode(DIRECTORY_SEPARATOR, $command));
        });
        echo $returnCode ? "=== ERROR ===\n" : "=== SUCCESS ===\n";
        return $returnCode;
    }

    private function exec($command)
    {
        exec($command, $opt, $returnCode);
        echo implode($opt, "\n") . "\n";
        return $returnCode;
    }
}
