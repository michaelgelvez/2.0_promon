@extends('layouts.admin', [
    'title' => 'Promon',
    'page_name' => 'applicant-list',
])

@section('content')
<div class="breadcrumbs__container clearfix">
    <div class="breadcrumbs">
        <ul>
            <li class="active"><a href="#">募集一覧</a></li>
        </ul>
    </div>
    <div class="breadcrumbs__btnContainer">
        <a href="{{ route('applicants.export', Request::route('id')) }}" class="bc__button">CSVダウンロード</a>
    </div>
</div> <!-- .breadcrumbs__container -->
<div class="section form addPad2">
    <div class="section__row">
        <form id="form_title" method="POST">
            <select id="select_title" name="id" onChange="onChange_Form_TitleSelection(this)">
                <option value="" selected disabled>選択してください</option>
                @foreach($forms as $form)
                <option value="{{ $form->id }}" {{ Request::route('id') == $form->id ? 'selected' : '' }}>{{ $form->title }}</option>
                @endforeach
            </select>
        </form>
    </div>
    <div class="section__row">
        <table id="dt-basic" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    @foreach($questions as $key => $question)
                    <td>{{ $question }}</td>
                    @endforeach
                </tr>
            </thead>
            <tbody>
            @foreach($answers as $answerkey => $answer)
            <tr>
                @foreach($questions as $questionKey => $question)
                <td>{{ isset($answer[$questionKey]) ? $answer[$questionKey] : '---' }}</td>   
                @endforeach
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@push('scripts')
<script>
    function onChange_Form_TitleSelection(e){
        let form = $(e).closest('form');
        var url = "{{ route('applicants.post', ['id' => '_id' ]) }}".replace('_id', e.value);
        form.attr('action', url);
        form.submit();
    };
    $(function(){
        $('#form_title').prepend('<input type="hidden" name="_token" value="{{ csrf_token() }}">');
        var form_title_id = $('#select_title').val()
        if(!form_title_id){
            $('.breadcrumbs__btnContainer').addClass('hide');
        }
    });
</script>
<script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
@endpush
