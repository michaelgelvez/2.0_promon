@extends('layouts.admin', [
    'title' => 'Promon | フォーム 一覧',
    'page_name' => 'form_create',
])

@section('content')
<div class="breadcrumbs__container clearfix">
    <div class="breadcrumbs">
        <ul>
            <li>フォーム</li>
            <li class="active"><a href="#">新規作成</a></li>
        </ul>
    </div>
</div> <!-- .breadcrumbs__container -->
<form method="post" action="{{ route('forms.save', $form->id ?? null ) }}">
@csrf
    <div class="section form nobg">
        <div class="section__row">
            <p class="section__label title">タイトル<i class="required">*</i></p>
        </div>
        <div class="section__row">
            <input type="text" name="title" value="{{ $form['title'] ?: '' }}" placeholder="COMMUDE">
        </div> <!-- .section__row -->
    </div><!-- .section -->
    <div class="breadcrumbs__container clearfix">
        <div class="breadcrumbs__btnContainer">
            <a onClick="onClick_Questions_AddSection(this)" class="bc__button"><span class="add"></span>設問追加</a>
        </div>
    </div>
    @forelse ($form['questions'] ?: [] as $item => $question)
    <div class="section question" data-id="{{  explode('-', $item, 2)[1]  ?? 1 }}">
        <div class="section__row">
            <p class="section__label title">
                設問タイトルがこちらに入ります
            </p>
            <div class="buttonContainer">
                <a href="javascript:void(0);"><span class="toggleArrow" onclick="onClick_questionToggle(this)"></span></a>
                <a href="javascript:void(0);"><span class="closeBtn" onclick="onClick_Questions_DelSection(this)"></span></a>
            </div>
        </div>
        <div class="section__content">
            <div class="section__content--table">
                <div class="tr questionField">
                    <div class="th">設問<i class="required">*</i></div>
                    <div class="td">
                        <textarea name="questions[question-{{  explode('-', $item, 2)[1]  ?? 1 }}][question]">{{ $question['question'] ?? '' }}</textarea>
                    </div>
                </div>
                <div class="tr">
                    <div class="th">タイプ<i class="required">*</i></div>
                    <div class="td">
                        <select class="select--type" name="questions[question-{{  explode('-', $item, 2)[1]  ?? 1 }}][type]" onChange="onChange_Fields_TypeSelection(this)">
                            <option data-fieldtype="" value="選択してください" selected disabled>選択してください</option>
                            <option data-fieldtype="text" value="text" {{ isset($question['type']) && $question['type'] == 'text' ? 'selected' : '' }}>TEXT</option>
                            <option data-fieldtype="email" value="email" {{ isset($question['type']) && $question['type'] == 'email' ? 'selected' : '' }}>EMAIL</option>
                            <option data-fieldtype="textarea" value="textarea" {{ isset($question['type']) && $question['type'] == 'textarea' ? 'selected' : '' }}>TEXTAREA</option>
                            <option data-fieldtype="radiobutton" value="radio" {{ isset($question['type']) && $question['type'] == 'radio' ? 'selected' : '' }}>RADIO</option>
                            <option data-fieldtype="select" value="select" {{ isset($question['type']) && $question['type'] == 'select' ? 'selected' : '' }}>SELECTION</option>
                            <option data-fieldtype="checkbox" value="checkbox" {{ isset($question['type']) && $question['type'] == 'checkbox' ? 'selected' : '' }}>CHECKBOX</option>
                        </select>
                    </div>
                </div>
                <div class="tr questiontypeField">
                    <div class="th">タイプ<i class="required">*</i></div>
                    <div class="td">
                        <select class="select--inputID" name="questions[question-{{  explode('-', $item, 2)[1]  ?? 1 }}][master_input_id]"
                        onClick="onClick_Fields_InputSelection()" data-id="{{ $question['master_input_id'] ?? '' }}">
                            <option data-fieldtype="" value="選択してください" selected disabled>選択してください</option>
                        </select>
                    </div>
                </div>
                <div class="tr requiredField">
                    <div class="th">必須</div>
                    <div class="td">
                        <div class="custom__checkbox">
                            <label>
                                <input type="checkbox" value="1" {{ isset($question['is_required']) && $question['is_required'] == 1 ? 'checked' : '' }} name="questions[question-{{  explode('-', $item, 2)[1]  ?? 1 }}][is_required]">
                                <span class="btn"></span>
                            </label>
                        </div>
                        <p>この設問を解凍必須にする</p>
                    </div>
                </div>
                <div class="tr requiredField hasOthers hide">
                    <div class="th">その他</div>
                    <div class="td">
                        <div class="custom__checkbox">
                            <label>
                                <input type="checkbox" value="1" {{ isset($question['has_others']) && $question['has_others'] == 1 ? 'checked' : '' }} name="questions[question-{{  explode('-', $item, 2)[1]  ?? 1 }}][has_others]">
                                <span class="btn"></span>
                            </label>
                        </div>
                        <p>その他のフィールドを追加</p>
                    </div>
                </div>
                <div class="tr">
                    <div class="th">設問前</div>
                    <div class="td">
                        <textarea name="questions[question-{{  explode('-', $item, 2)[1]  ?? 1 }}][before_text]">{{ $question['before_text'] ?? '' }}</textarea>
                    </div>
                </div>
                <div class="tr">
                    <div class="th">設問後</div>
                    <div class="td">
                        <textarea name="questions[question-{{  explode('-', $item, 2)[1]  ?? 1 }}][after_text]">{{ $question['after_text'] ?? '' }}</textarea>
                    </div>
                </div>
            </div> <!-- .section__content--table -->
        </div> <!-- .section__content -->
    </div><!-- .section question-->
    @empty
    <div class="section question" data-id="1">
        <div class="section__row">
            <p class="section__label title">
                設問タイトルがこちらに入ります
            </p>
            <div class="buttonContainer">
                <a href="javascript:void(0);"><span class="toggleArrow" onclick="onClick_questionToggle(this)"></span></a>
                <a href="javascript:void(0);"><span class="closeBtn" onclick="onClick_Questions_DelSection(this)"></span></a>
            </div>
        </div>
        <div class="section__content">
            <div class="section__content--table">
                <div class="tr questionField">
                    <div class="th">設問<i class="required">*</i></div>
                    <div class="td">
                        <textarea name="questions[question-1][question]"></textarea>
                    </div>
                </div>
                <div class="tr">
                    <div class="th">タイプ<i class="required">*</i></div>
                    <div class="td">
                        <select class="select--type" name="questions[question-1][type]" onChange="onChange_Fields_TypeSelection(this)">
                            <option data-fieldtype="" value="選択してください" selected disabled>選択してください</option>
                            <option data-fieldtype="text" value="text">TEXT</option>
                            <option data-fieldtype="email" value="email">EMAIL</option>
                            <option data-fieldtype="textarea" value="textarea">TEXTAREA</option>
                            <option data-fieldtype="radiobutton" value="radio">RADIO</option>
                            <option data-fieldtype="select" value="select">SELECTION</option>
                            <option data-fieldtype="checkbox" value="checkbox">CHECKBOX</option>
                        </select>
                    </div>
                </div>
                <div class="tr questiontypeField">
                    <div class="th">タイプ<i class="required">*</i></div>
                    <div class="td">
                        <select class="select--inputID" name="questions[question-1][master_input_id]"
                        onClick="onClick_Fields_InputSelection()" data-id="">
                            <option data-fieldtype="" value="選択してください" selected disabled>選択してください</option>
                        </select>
                    </div>
                </div>
                <div class="tr requiredField">
                    <div class="th">必須</div>
                    <div class="td">
                        <div class="custom__checkbox">
                            <label>
                                <input type="checkbox" value="1" name="questions[question-1][is_required]">
                                <span class="btn"></span>
                            </label>
                        </div>
                        <p>この設問を解凍必須にする</p>
                    </div>
                </div>
                <div class="tr requiredField hasOthers hide">
                    <div class="th">その他</div>
                    <div class="td">
                        <div class="custom__checkbox">
                            <label>
                                <input type="checkbox" value="1" name="questions[question-1][has_others]">
                                <span class="btn"></span>
                            </label>
                        </div>
                        <p>その他のフィールドを追加</p>
                    </div>
                </div>
                <div class="tr">
                    <div class="th">設問前</div>
                    <div class="td">
                        <textarea name="questions[question-1][before_text]"></textarea>
                    </div>
                </div>
                <div class="tr">
                    <div class="th">設問後</div>
                    <div class="td">
                        <textarea name="questions[question-1][after_text]"></textarea>
                    </div>
                </div>
            </div> <!-- .section__content--table -->
        </div> <!-- .section__content -->
    </div><!-- .section question-->
    @endforelse
    <div class="section padded">
        <div class="section__row">
            <p class="section__label title">設定</p>
            <div class="buttonContainer">
                <a href="javascript:void(0);"><span class="toggleArrow" onClick="onClick_configToggle(this)"></span></a>
            </div>
        </div>
        <div class="section__content">
            <div class="section__content--table">
                <div class="tr">
                    <div class="th">応募完了メール</div>
                    <div class=" td">
                        <div class="row">
                            <div class="custom__radioBtnbox">
                                <label>
                                    <input type="radio" name="admin_is_notify" value="1" {{ $form['admin_is_notify'] == 1 ? 'checked' : '' }} onclick="show_And_Hide_AdminForm()">
                                    <span class="btn"></span>
                                    <p>管理通知メールを利用する</p>
                                </label>
                            </div>
                            <div class="custom__radioBtnbox">
                                <label>
                                    <input type="radio" name="admin_is_notify" value="0" {{ $form['admin_is_notify'] == 0 ? 'checked' : '' }} onclick="show_And_Hide_AdminForm()">
                                    <span class="btn"></span>
                                    <p>管理通知メールを利用しない</p>
                                </label>
                            </div>
                        </div>
                            <div class="row adminForm">
                                <p>件名 <span>※未設定の場合、「アンケートタイトル」への回答がありました。となります</span><i class="required">*</i></p>
                            </div>
                            <div class="row adminForm">
                                <input type="text" name="admin_subject" value="{{ $form['admin_subject'] ?: '' }}">
                            </div>
                            <div class="row adminForm">
                                <p>メールアドレス <span>※改行で区切って複数入力できます。</span><i class="required">*</i></p>
                            </div>
                            @forelse($form['admin_emails'] ?: [] as $key => $value)
                            <div class="row adminEmail adminForm">
                                <input type="text" name="admin_emails[]" value="{{ $value }}">
                                <div class="section__configCol txtadd"><a href="javascript:void(0);" onclick="onClick_Admin_Email_AddTxtElement(this)">
                                    <p>+</p></a>
                                </div>
                                <div class="section__configCol txtdel"><a href="javascript:void(0);" onclick="onClick_Admin_Email_DelTxtElement(this)">
                                    <p>-</p></a>
                                </div>
                            </div>
                            @empty
                            <div class="row adminEmail adminForm">
                                <input type="text" name="admin_emails[]" value="">
                                <div class="section__configCol txtadd"><a href="javascript:void(0);" onclick="onClick_Admin_Email_AddTxtElement(this)">
                                    <p>+</p></a>
                                </div>
                                <div class="section__configCol txtdel"><a href="javascript:void(0);" onclick="onClick_Admin_Email_DelTxtElement(this)">
                                    <p>-</p></a>
                                </div>
                            </div>
                            @endforelse
                    </div>
                </div>
                <div class="tr">
                    <div class="th">応募完了メール</div>
                    <div class="td">
                        <div class="row">
                            <div class="custom__radioBtnbox">
                                <label>
                                    <input type="radio" name="user_is_notify" value="1" {{ $form['user_is_notify'] == 1 ? 'checked' : '' }} onclick="show_And_Hide_UserForm()">
                                    <span class="btn"></span>
                                    <p>応募完了メールを利用する</p>
                                </label>
                            </div>
                            <div class="custom__radioBtnbox">
                                <label>
                                    <input type="radio" name="user_is_notify" value="0" {{ $form['user_is_notify'] == 0 ? 'checked' : '' }} onclick="show_And_Hide_UserForm()">
                                    <span class="btn"></span>
                                    <p>応募完了メールを利用しない</p>
                                </label>
                            </div>
                        </div>
                        <div class="row userForm">
                            <p>件名 <span>※未設定の場合、「アンケートタイトル」への回答がありました。となります</span><i class="required">*</i></p>
                        </div>
                        <div class="row userForm">
                            <input type="text" name="user_subject" value="{{ $form['user_subject'] }}">
                        </div>
                        <div class="row userForm">
                            <p>送信元メールアドレス<i class="required">*</i></p>
                        </div>
                        @forelse($form['user_emails'] ?: [] as $key => $value)
                        <div class="row userEmail userForm">
                            <input type="text" name="user_emails[]" value="{{ $value }}">
                            <div class="section__configCol txtadd"><a href="javascript:void(0);" onclick="onClick_User_Email_AddTxtElement(this)">
                                <p>+</p></a>
                            </div>
                            <div class="section__configCol txtdel"><a href="javascript:void(0);" onclick="onClick_User_Email_DelTxtElement(this)">
                                <p>-</p></a>
                            </div>
                        </div>
                        @empty
                        <div class="row userEmail userForm">
                            <input type="text" name="user_emails[]" value="">
                            <div class="section__configCol txtadd"><a href="javascript:void(0);" onclick="onClick_User_Email_AddTxtElement(this)">
                                <p>+</p></a>
                            </div>
                            <div class="section__configCol txtdel"><a href="javascript:void(0);" onclick="onClick_User_Email_DelTxtElement(this)">
                                <p>-</p></a>
                            </div>
                        </div>
                        @endforelse
                        <div class="row userForm">
                            <p>文章<i class="required">*</i></p>
                        </div>
                        <div class="row userForm">
                            <textarea name="user_email_content">{{ $form['user_email_content'] ?: '' }}</textarea>
                        </div>
                    </div>
                </div>
            </div> <!-- .section__content--table -->
        </div> <!-- .section__content -->
    </div><!-- .section -->
    <div class="btn__container">
        <button type="submit" class="btn__container">更新</button>
    </div> <!-- .btn__container -->
</form>
@endsection
@push('scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js"></script>
<script src="{{ asset('js/forms.js') }}"></script>
<script>
var masterInputs = {!! json_encode($masterInputs -> toArray())!!};
@if ($errors->any())
  alert('{{ $errors->first('*') }}');
@endif
</script>
@endpush
