@extends('layouts.admin', [
'title' => 'Promon',
'page_name' => 'member-list',
])

@section('content')
<div class="breadcrumbs__container clearfix">
    <div class="breadcrumbs">
        <ul>
            <li>募集一覧</li>
            <li class="active"><a href="#">新規作成</a></li>
        </ul>
    </div>
</div> <!-- .breadcrumbs__container -->
<form method="POST" action="{{ route('members.save', Request::route('id') ?? null) }}" id="pageForm">
    @csrf
    <div class="section form">
        <div class="section__content no-top">
            <div class="section__content--table">
            {!! $content !!}
            </div> <!-- .section__content--table -->
        </div> <!-- .section__content -->
    </div><!-- .section -->
    <div class="btn__container">
        <button type="submit" class="btn__container">更新</button>
    </div> <!-- .btn__container -->
</form>
@endsection
@push('scripts')
<script>

function enableDisableButton(e){
    var textbox = document.getElementsByClassName(e);
    if(textbox[0].disabled){
        textbox[0].disabled = false;
    }else{
        textbox[0].disabled = true;
    }
}

@if ($errors->any())
  alert('{{ $errors->first('*') }}');
@endif
</script>
@endpush
