@extends('layouts.admin', [
'title' => 'Promon',
'page_name' => 'member-list',
])

@section('content')
<div class="breadcrumbs__container clearfix">
    <div class="breadcrumbs">
        <ul>
            <li class="active"><a href="#">募集一覧</a></li>
        </ul>
    </div>
    <div class="breadcrumbs__btnContainer">
        <a href="{{ route('members.export') }}" class="bc__button">CSVダウンロード</a>
    </div>
    <div class="breadcrumbs__btnContainer">
            <a href="{{ route('members.create') }}" class="bc__button">+新規作成</a>
        </div>
</div> <!-- .breadcrumbs__container -->
<form method="post" action="{{ route('members.delete') }}">
@csrf
    <div class="section nosidepad">
        <div class="section__row">
            <div class="pages">
                <div class="thead all-pages">
                    <div class="tr">
                        <div class="td">
                            <div class="custom__checkbox">
                                <label>
                                    <input type="checkbox" name="page_all" id="selectAllBtn" value="1">
                                    <span class="btn"></span>
                                </label>
                            </div>
                            <p class="checkbox__label">一括削除</p>
                        </div>
                        <div class="td">
                            <p>&nbsp;</p>
                        </div>
                        <div class="td">
                            <p>&nbsp;</p>
                        </div>
                        <div class="td trash">
                            <button type="submit" id="btnSubmit" disabled><img src="{{ asset('images/icons/icon_trash.png') }}" alt=""></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section form addPad2">
        <div class="pages">
            <div class="section__row">
                <table id="dt-basic-checkbox" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            @foreach (array_except($members->getTableColumns(), 0) as $memberHeader)
                            <th class="th-sm">{{ $memberHeader }}</th>
                            @endforeach
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($members->all() as $member)
                    <tr>
                        <td>
                        <div class="custom__checkbox">
                                <label>
                                    <input type="checkbox" name="id[]" class="chkBoxId" value="{{ $member->id }}">
                                    <span class="btn"></span>
                                </label>
                            </div>
                        </td>
                        @foreach(array_except($member->find($member->id)->getAttributes(), 'id') as $key => $item)
                        <td>
                            <p class="data__label">
                            {{ preg_match('/^\$2y\$/', $item ) ? '<<!!Encrypted!!>>' : $item }}
                            </p>
                        </td>
                        @endforeach
                        <td><div class="pages__Btn"><a href="{{ route('members.edit', $member->id) }}"><p>編集</p></a></div></td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div> <!-- .section__content -->
        </div>
    </div><!-- .section -->
</form>
@endsection
@push('scripts')
<script>
    $('#selectAllBtn').click(function(event) {
        $(".chkBoxId").prop('checked', $(this).prop('checked'));
        chkboxCheck();
    });

    $('.chkBoxId').change(function () {
        chkboxCheck();
    });

    function chkboxCheck()
    {
        var chkBox = $('input.chkBoxId:checkbox:checked').length > 0;
        if(chkBox){
            $('#btnSubmit').attr('disabled', false);
        } else {
            $('#btnSubmit').attr('disabled', true);
        }
    }
</script>
<script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/mdbootstrap/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/mdbootstrap/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/mdbootstrap/mdb.min.js') }}"></script>
@endpush
