<header>
    <h1 class="logo">
        <a href="#">
            <img src="{{ asset('images/logo.svg') }}" alt="logo">
        </a>
    </h1>
    <div class="logout">
        <a href="{{ route('logout') }}">
            <img src="{{ asset('images/icons/icon_logout.png') }}" alt="logout">
        </a>
    </div>
</header>
