<aside id="navi">
    <nav class="pc">
        <ul class="">
            <li class="dashboard {{  $page_name == 'dashboard' ? 'active' : '' }}">
                <a href="{{ route('dashboard') }}">
                    <img src="{{ asset('images/icons/icon_dashboard.png') }}" alt="navicon">
                    <p>ダッシュボード</p>
                </a>
            </li>
            <li class="page dropdown {{ $page_name == 'page_index' ? 'active' : ''}} {{ $page_name == 'page_create' ? 'js-dropdown' : '' }}">
                <a href="{{ route('pages.index') }}">
                    <img src="{{ asset('images/icons/icon_page.png') }}" alt="navicon">
                    <p>ページ</p>
                </a>
                <div class="dropdownBtn">
                    <a href="javascript:void(0);"><span></span></a>
                </div>
                <ul class="submenu">
                    <li class={{ $page_name == 'page_create' ? 'active' : '' }}><a href="{{ route('pages.create') }}"><p>新規作成</p></a></li>
                </ul>
            </li>
            <li class="form {{ $page_name == 'form_index' || $page_name == 'form_create' ? 'active' : '' }}">
                <a href="{{ route('forms.index') }}">
                    <img src="{{ asset('images/icons/icon_form.png') }}" alt="navicon">
                    <p>フォーム</p>
                </a>
            </li>
            <li class="design {{ $page_name == 'design' ? 'active' : '' }}" >
                <a href="{{ route('design.index') }}">
                    <img src="{{ asset('images/icons/icon_design.png') }}" alt="navicon">
                    <p>デザイン</p>
                </a>
            </li>
            <li class="analytics {{ $page_name == 'analytics' ? 'active' : '' }}">
                <a href="{{ route('analytics') }}">
                    <img src="{{ asset('images/icons/icon_analytics.png') }}" alt="navicon">
                    <p>アナリティクス</p>
                </a>
            </li>
            <li class="list {{ $page_name == 'applicant-list' ? 'active' : '' }}">
                <a href="{{ route('applicants') }}">
                    <img src="{{ asset('images/icons/icon_list.png') }}" alt="navicon">
                    <p>募集一覧</p>
                </a>
            </li>
            <li class="list {{ $page_name == 'member-list' ? 'active' : '' }}">
                <a href="{{ route('members.index') }}">
                    <img src="{{ asset('images/icons/icon_list.png') }}" alt="navicon">
                    <p>メンバー覧</p>
                </a>
            </li>
            <li class="settings {{ $page_name == 'settings' ? 'active' : '' }}">
                <a href="{{ route('settings.index') }}">
                    <img src="{{ asset('images/icons/icon_settings.png') }}" alt="navicon">
                    <p>設定</p>
                </a>
            </li>
        </ul>
    </nav>
</aside><!-- .navi -->
