@extends('layouts.app', [
    'title' => 'Promon | 管理画面',
    'static_class' => 'login',
    'footer_class' => 'footer--login',])

@section('content')
<div class="login">
    <div class="login">
        <div class="logo">
            <figure>
                <img src="{{ asset('images/logo.svg') }}" alt="">
            </figure>
        </div>
        <form method="POST" action="{{ route('password.update') }}">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form__wrapper">
                <input type="hidden" name="email" value="{{ $email ?? old('email') }}" placeholder="メールアドレスを入力してください。" required autocomplete="email" autofocus>
                <div class="input password clearfix">
                    <figure>
                        <img src="{{ asset('images/icons/icon_login_password.png') }}" alt="">
                    </figure>
                    <input type="password" name="password" autocomplete="new-password" placeholder="新しいパスワードの入力">
                </div>
                <div class="input password clearfix">
                    <figure>
                        <img src="{{ asset('images/icons/icon_login_password.png') }}" alt="">
                    </figure>
                    <input type="password" name="password_confirmation" autocomplete="new-password" placeholder="新しいパスワードの確認入力">
                    </div>
                <div class="btn--wrapper">
                    <input type="submit" class="btn--login" value="新しいバスワードを取得">
                </div>
            </div><!-- .form__wrapper -->
        </form>
    </div>
</div>
@endsection
@push('scripts')
<script>
@if ($errors->any())
  alert('{{ $errors->first('*') }}');
@endif
</script>
@endpush
