@extends('layouts.app', [
    'title' => 'Promon | 管理画面',
    'static_class' => 'login',
    'footer_class' => 'footer--login',])

@section('content')
<div class="login">
    <div class="login">
        <div class="logo">
            <figure>
                <img src="{{ asset('images/logo.svg') }}" alt="">
            </figure>
        </div>
        <form method="POST" action="{{ route('password.email') }}">
            @csrf
            <div class="form__wrapper">
                <div class="input user clearfix">
                    <figure>
                        <img src="{{ asset('images/icons/icon_login_user.png') }}" alt="">
                    </figure>
                    <input type="text" name="email" value="{{ old('email') }}" placeholder="メールアドレスを入力してください。" autocomplete="email" autofocus>
                </div>
                <div class="btn--wrapper">
                    <input type="submit" class="btn--login" value="新しいバスワードを取得">
                </div>
            </div><!-- .form__wrapper -->
        </form>
    </div>
</div>
@endsection
@push('scripts')
<script>
@if ($errors->any())
  alert('{{ $errors->first('*') }}');
@endif
@if (session('status'))
  alert('{{ session('status') }}');
@endif
</script>
@endpush
