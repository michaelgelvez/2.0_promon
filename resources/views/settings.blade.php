@extends('layouts.admin', [
    'title' => 'Promon | 設定',
    'page_name' => 'settings',
])

@section('content')
<div class="breadcrumbs__container clearfix">
    <div class="breadcrumbs">
        <ul>
            <li class="active"><a href="#">設定</a></li>
        </ul>
    </div>
</div> <!-- .breadcrumbs__container -->
<div class="section form">
    <div class="section__content no-top">
        <div class="section__content--table">
            <div class="tr configNav js-configNav">
                <div class="td no-top">
                    <a href="#campaign" class="configNav__item" id="configNav-details"><p>キャンペーン詳細</p></a>
                </div>
                <div class="td no-top">
                    <a href="#fieldmaster" class="configNav__item" id="configNav-field"><p>フィールドマスタ</p></a>
                </div>
                <div class="td no-top">
                    <a href="#members" class="configNav__item" id="configNav-userlink"><p>ユーザ紐付け機能</p></a>
                </div>
                <div class="td no-top">
                    <a href="#security" class="configNav__item" id="configNav-security"><p>セキュリティ</p></a>
                </div>
            </div>
        </div> <!-- .section__content--table -->
    </div> <!-- .section__content -->
</div>
<div class="section form section__config" id="config-details">
    <div class="section__configInner">
        <form method="POST" action="{{ route('settings.campaign.store') }}">
            @csrf
            <div class="section__configRow">
                <div class="section__configCol label">
                    <p>キャンペーン名<i class="required">*</i></p>
                </div>
                <div class="section__configCol txtfld">
                    <input type="text" name="name" value="{{ $campaign['name'] }}">
                </div>
            </div>
            <div class="section__configRow">
                <div class="section__configCol label">
                    <p>公開期間</p>
                </div>
                <div class="section__configCol short">
                    <input type="text" name="released_start_at" class="datepicker" readonly="readonly" value="{{ is_null($campaign['released_start_at']) ? '' : $campaign['released_start_at']->format('m/d/Y') }}">
                </div>
                <div class="section__configCol txtbetween"><p>～</p></div>
                <div class="section__configCol short">
                    <input type="text" name="released_end_at" class="datepicker" readonly="readonly" value="{{ is_null($campaign['released_end_at']) ? '' : $campaign['released_end_at']->format('m/d/Y') }}">
                </div>
            </div>
            <div class="section__configRow labelTop">
                <div class="section__configCol label">
                    <p>終了後のリダイレクト先URL<i class="required">*</i></p>
                </div>
                <div class="section__configCol full">
                    <input type="text" name="finish_redirect_url" value="{{ $campaign['finish_redirect_url'] }}" placeholder="https://honda-promon.com.jp">
                </div>
            </div>

            <div class="btn__container">
                @if(Session::has('success_campaign'))
                <span class="success">{{ Session::get('success_campaign') }}</span>
                @endif
                <button type="submit" class="btn__container">更新</button>
            </div> <!-- .btn__container -->
        </form>
    </div>
</div>
<div class="section form section__config" id="config-field">
    <div class="section__config--btnContainer">
        <a class="button" onclick="onClick_Field_AddSectionElement()">マスタフィールド追加</a>
    </div>
    <form method="post" action="{{ route('settings.fields.save') }}">
    @csrf
        <div class="section__configInner">
            @forelse($masterFields->values() as $key => $item)
            <div class="section__configRowContainer top-border" id="field-{{ $item['id'] }}" data-id="{{ $item['id'] }}">
                <div class="section__configRow">
                    <div class="section__config--btnContainer delete">
                        <a class="button" onclick="onClick_Field_DelSectionElement(this)">マスターフィールドを削除</a>
                    </div>
                </div>
                <div class="section__configRow">
                    <div class="section__configCol label">
                        <p>マスターフィールドタイプ<i class="required">*</i></p>
                    </div>
                    <div class="section__configCol short">
                        <select name="field-{{ $item['id'] }}[type]" onChange="onChange_Fields_TypeSelection(this)">
                            <option value="text" {{ $item['type'] == 'text' ? 'selected' : '' }}>TEXT</option>
                            <option value="email" {{ $item['type'] == 'email' ? 'selected' : '' }}>EMAIL</option>
                            <option value="radio" {{ $item['type'] == 'radio' ? 'selected' : '' }}>RADIO</option>
                            <option value="checkbox" {{ $item['type'] == 'checkbox' ? 'selected' : '' }}>CHECKBOX</option>
                            <option value="select" {{ $item['type'] == 'select' ? 'selected' : '' }}>SELECTION</option>
                            <option value="textarea" {{ $item['type'] == 'textarea' ? 'selected' : '' }}>TEXTAREA</option>
                        </select>
                    </div>
                </div>
                <div class="section__configRow">
                    <div class="section__configCol label">
                        <p>マスタフィールド名<i class="required">*</i></p>
                    </div>
                    <div class="section__configCol txtfld">
                        <input type="text" name="field-{{ $item['id'] }}[name]" value="{{ $item['name'] }}">
                    </div>
                </div>
                <div class="section__configRow labels {{ (in_array($item['type'], ['text','email','textarea'])) ? '' : 'hide' }}">
                    <div class="section__configCol label">
                        <p>フィールドマスター</p>
                    </div>
                    <div class="section__configCol txtfld">
                        <input type="text" name="field-{{ $item['id'] }}[label]" value="{{ $item['label'] }}">
                    </div>
                </div>
                @foreach(json_decode($item['options'] ?? '[""]') as $option)
                <div class="section__configRow selections {{ (in_array($item['type'], ['text','email','textarea'])) ? 'hide' : '' }}">
                    <div class="section__configCol label">
                        <p>選択肢<i class="required">*</i></p>
                    </div>
                    <div class="section__configCol label">
                        <input type="text" name="field-{{ $item['id'] }}[options][]" value="{{ $option }}">
                    </div>
                    <div class="section__configCol txtadd"><a href="javascript:void(0);" onclick="onClick_Field_Option_AddTxtElement(this)">
                        <p>+</p></a>
                    </div>
                    <div class="section__configCol txtdel"><a href="javascript:void(0);" onclick="onClick_Field_Option_DelTxtElement(this)">
                        <p>-</p></a>
                    </div>
                </div>
                @endforeach
            </div>
            @empty
            <div class="section__configRowContainer" id="newField-1" data-id="1">
                <div class="section__configRow">
                    <div class="section__config--btnContainer delete">
                        <a class="button" onclick="onClick_Field_DelSectionElement(this)">マスターフィールドを削除</a>
                    </div>
                </div>
                <div class="section__configRow">
                    <div class="section__configCol label">
                        <p>マスターフィールドタイプ<i class="required">*</i></p>
                    </div>
                    <div class="section__configCol short">
                        <select name="newField-1[type]" onChange="onChange_Fields_TypeSelection(this)">
                            <option value="" selected>選択してください</option>
                            <option value="text" >TEXT</option>
                            <option value="email">EMAIL</option>
                            <option value="radio">RADIO</option>
                            <option value="checkbox">CHECKBOX</option>
                            <option value="select">SELECTION</option>
                            <option value="textarea">TEXTAREA</option>
                        </select>
                    </div>
                </div>
                <div class="section__configRow">
                    <div class="section__configCol label">
                        <p>マスタフィールド名<i class="required">*</i></p>
                    </div>
                    <div class="section__configCol txtfld">
                        <input type="text" name="newField-1[name]">
                    </div>
                </div>
                <div class="section__configRow labels">
                    <div class="section__configCol label">
                        <p>フィールドマスター</p>
                    </div>
                    <div class="section__configCol txtfld">
                        <input type="text" name="newField-1[label]">
                    </div>
                </div>
                <div class="section__configRow selections hide">
                    <div class="section__configCol label">
                        <p>選択肢<i class="required">*</i></p>
                    </div>
                    <div class="section__configCol label">
                        <input type="text" name="newField-1[options][]">
                    </div>
                    <div class="section__configCol txtadd"><a href="javascript:void(0);" onclick="onClick_Field_Option_AddTxtElement(this)">
                        <p>+</p></a>
                    </div>
                    <div class="section__configCol txtdel"><a href="javascript:void(0);" onclick="onClick_Field_Option_DelTxtElement(this)">
                        <p>-</p></a>
                    </div>
                </div>
            </div>
            @endforelse
        </div>
        <div class="section__configInner top-border">
            <div class="btn__container">
                @if(Session::has('success_masterField'))
                <span class="success">{{ Session::get('success_masterField') }}</span>
                @endif
                <button type="submit" class="btn__container">更新</button>
            </div> <!-- .btn__container -->
        </div>
    </form>
</div>
<div class="section form section__config" id="config-userlink">
    <div class="section__configInner">
        <form method="post" action="{{ route('settings.user.save') }}">
            @csrf
            @forelse($memberHeaders->values() as $key => $item)
            <div class="section__configRowContainer" data-id="{{ $item['id'] }}">
                <div class="section__configRow">
                    <div class="section__configCol label">
                        <p>フィールド名<i class="required">*</i></p>
                    </div>
                    <div class="section__configCol txtfld">
                        <input type="text" name="user-{{ $item['id'] }}[field]" value="{{ $item['field'] }}">
                    </div>
                    <div class="section__configCol txtadd"><a href="javascript:void(0);" onclick="onClick_User_AddSectionElement(this)">
                        <p>+</p></a>
                    </div>
                    <div class="section__configCol txtdel"><a href="javascript:void(0);" onclick="onClick_User_DelSectionElement(this)">
                        <p>-</p></a>
                    </div>
                </div>
                <div class="section__configRow">
                    <div class="custom__radioBtnbox">
                        <label>
                            <input type="radio" name="user-{{ $item['id'] }}[is_encrypted]" value="0" {{ $item['is_encrypted'] ? '' : 'checked' }}>
                            <span class="btn"></span>
                            <p>ワード</p>
                        </label>
                    </div>
                    <div class="custom__radioBtnbox">
                        <label>
                            <input type="radio" name="user-{{ $item['id'] }}[is_encrypted]" value="1" {{ $item['is_encrypted'] ? 'checked' : '' }}>
                            <span class="btn"></span>
                            <p>暗号化</p>
                        </label>
                    </div>
                </div>
            </div>
            @empty
            <div class="section__configRowContainer" data-id="1">
                <div class="section__configRow">
                    <div class="section__configCol label">
                        <p>フィールド名<i class="required">*</i></p>
                    </div>
                    <div class="section__configCol txtfld">
                        <input type="text" name="user-1[field]">
                    </div>
                    <div class="section__configCol txtadd"><a href="javascript:void(0);" onclick="onClick_User_AddSectionElement(this)">
                        <p>+</p></a>
                    </div>
                    <div class="section__configCol txtdel"><a href="javascript:void(0);" onclick="onClick_User_DelSectionElement(this)">
                        <p>-</p></a>
                    </div>
                </div>
                <div class="section__configRow">
                    <div class="custom__radioBtnbox">
                        <label>
                            <input type="radio" name="user-1[is_encrypted]" value="0" checked>
                            <span class="btn"></span>
                            <p>ワード</p>
                        </label>
                    </div>
                    <div class="custom__radioBtnbox">
                        <label>
                            <input type="radio" name="user-1[is_encrypted]" value="1">
                            <span class="btn"></span>
                            <p>暗号化</p>
                        </label>
                    </div>
                </div>
            </div>
            @endforelse
            <div class="btn__container">
                @if(Session::has('success_member'))
                <span class="success">{{ Session::get('success_member') }}</span>
                @endif
                <button type="submit" class="btn__container">更新</button>
            </div> <!-- .btn__container -->
        </form>
    </div>
    <div class="section__configInner top-border">
        <div class="section__configRow">
            <form action="{{ route('settings.user.import') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="section__configCol label">
                <p>CSVインポート<i class="required">*</i></p>
                </div>
                <div class="section__configCol label">
                    <div class="inputfile-box">
                        <input type="file" id="file" name="file" class="inputfile" onchange='uploadFile(this)'>
                        <label for="file">
                        <span class="file-button">
                            <i class="fa fa-upload" aria-hidden="true"></i>
                            Select File
                            </span>
                            <span id="file-name" class="file-box"></span>
                        </label>
                    </div>
                </div>
                <div class="section__configCol label">
                </div>
                <button type="submit" class="configButton">参照ファイル</button>
            </form>
        </div>
    </div>
</div>
<div class="section form section__config" id="config-security">
    <div class="section__configInner">
        <form method="post" action="{{ route('settings.security.save') }}">
            @csrf
            @forelse($restrictAddresses->values() as $key => $item)
            <div class="section__configRowContainer" data-id="{{ $item->id }}">
                <div class="section__configRow">
                    <div class="section__configCol label">
                        <p>フィールド名</p>
                    </div>
                    <div class="section__configCol txtfld">
                        <input type="text" name="ip[]" value={{ $item->ip }}>
                    </div>
                    <div class="section__configCol txtadd">
                        <a href="javascript:void(0);" onclick="onClick_IP_AddTxtElement(this)">
                            <p>+</p>
                        </a>
                    </div>
                    <div class="section__configCol txtdel">
                        <a href="javascript:void(0);" onclick="onClick_IP_DelTxtElement(this)">
                            <p>-</p>
                        </a>
                    </div>
                </div>
            </div>
            @empty
            <div class="section__configRowContainer" data-id="1">
                <div class="section__configRow">
                    <div class="section__configCol label">
                        <p>フィールド名</p>
                    </div>
                    <div class="section__configCol txtfld">
                        <input type="text" name="ip[]">
                    </div>
                    <div class="section__configCol txtadd">
                        <a href="javascript:void(0);" onclick="onClick_IP_AddTxtElement(this)">
                            <p>+</p>
                        </a>
                    </div>
                    <div class="section__configCol txtdel">
                        <a href="javascript:void(0);" onclick="onClick_IP_DelTxtElement(this)">
                            <p>-</p>
                        </a>
                    </div>
                </div>
            </div>
            @endforelse
            <div class="btn__container">
                @if(Session::has('success_security'))
                <span class="success">{{ Session::get('success_security') }}</span>
                @endif
                <button type="submit" class="btn__container">更新</button>
            </div> <!-- .btn__container -->
        </form>
    </div>
</div>
@endsection
@push('scripts')
<script src="{{ asset('js/counterjquery/jquery.incremental-counter.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('js/datepicker.js') }}"></script>
<script src="{{ asset('js/page-config.js') }}"></script>
<script>
function uploadFile(e) {
	document.getElementById("file-name").innerHTML = e.files[0].name;
}
@if ($errors->any())
alert('{{ $errors->first('*') }}');
@endif
</script>
@endpush
