<!DOCTYPE html>
<html lang="ja">
@include('components.head')
<body>
    <div class="wrapper {{ $static_class ?? ''  }}">
        @yield('content')
        @include('components.footer')
    </div> <!-- .wrapper -->
    <!-- scripts -->
    @include('partials.script')
</body>

</html>
