@extends('layouts.admin', [
    'title' => 'Promon | ダッシュボード',
    'page_name' => 'dashboard',
])

@section('content')
<form method='post' action='#'>
@csrf
<div class="section">
    <div class="section__row">
        <p class="section__label">合計応募件数</p>
    </div>
    <div class="section__row">
        <div class="analyticsSelect dashboard">
            <select class="analyticsSelect" id="select_title" name="id" onChange="onChange_Form_TitleSelection(this)">
                <option value="" selected disabled>選択してください</option>
                @foreach($forms as $form)
                <option value="{{ $form->id }}" {{ Request::route('id') == $form->id ? 'selected' : '' }}>{{ $form->title }}</option>
                @endforeach
            </select>
        </div>
        <div class="incremental-counter" data-value="{{$analytics['submittedForms']}}">
        </div>
        <div class="chart">
            <canvas id="formChart"></canvas>
        </div>
    </div>
</div>
<div class="section">
    <div class="section__row">
        <p class="section__label">現在の表示 Promon</p>
    </div>
    <div class="section__row">
        <table id="dt-basic" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <td>キャンペーン名</td>
                    <td>公開期間</td>
                    <td>見せる</td>
                </tr>
            </thead>
            <tbody>
            @foreach($pages as $page)
            <tr>
                <td>{{ $campaign->name }}</td>
                <td>{{ $campaign->released_start_at->format('Y.m.d') }} ~ {{ $campaign->released_end_at->format('Y.m.d') }}</td>
                <td><a href="{{ route('dynamicForm.show', $page->permalink) }}" class="section__listItem--button" target="_blank">サイトをみる</a></td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@push('scripts')
<script>
    let chart_labels = {!! $analytics['labels'] !!};
    let chart_datasets = {!! json_encode($datasets) !!};
    let digitLength = {!! $analytics['submittedForms'] !!};

    function onChange_Form_TitleSelection(e){
        let form = $(e).closest('form');
        var url = "{{ route('dashboard.post', ['id' => '_id' ]) }}".replace('_id', e.value);
        form.attr('action', url);
        form.submit();
    };

    function numCounter(){
	$(".incremental-counter").incrementalCounter({
        "digits": digitLength.toString().length
        });
    }

    $(function(){
        chart();
        numCounter();
    });
</script>
<script src="{{ asset('js/counterjquery/jquery.incremental-counter.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script src="{{ asset('js/chart.js') }}"></script>
@endpush
