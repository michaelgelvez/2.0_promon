@extends('layouts.admin', [
    'title' => 'Promon | ページ 一覧',
    'page_name' => 'page_index',
])

@section('content')
<div class="breadcrumbs__container clearfix">
    <div class="breadcrumbs">
        <ul>
            <li class="active">ページ</li>
        </ul>
    </div>
    <div class="breadcrumbs__btnContainer">
        <a href="{{ route('pages.create') }}" class="bc__button"><span class="add"></span>新規作成</a>
    </div>
</div> <!-- .breadcrumbs__container -->
<form method="post" action="{{ route('pages.delete') }}">
@csrf
    <div class="section nosidepad">
        <div class="section__row">
            <div class="pages">
                <div class="thead all-pages">
                    <div class="tr">
                        <div class="td">
                            <div class="custom__checkbox">
                                <label>
                                    <input type="checkbox" name="page_all" id="selectAllBtn" value="1">
                                    <span class="btn"></span>
                                </label>
                            </div>
                            <p class="checkbox__label">一括削除</p>
                        </div>
                        <div class="td"><p>&nbsp;</p></div>
                        <div class="td"><p>&nbsp;</p></div>
                        <div class="td trash"><button type="submit" id="btnSubmit" disabled><img src="{{ asset('images/icons/icon_trash.png') }}" alt=""></button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section form addPad2">
        <div class="pages">
            <div class="section__row">
                <table id="dt-basic-checkbox" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <td></td>
                            <td>ページタイトル</td>
                            <td>作成日</td>
                            <td>ステータス</td>
                            <td>&nbsp;</td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($pages as $page)
                    <tr>
                        <td>
                            <div class="custom__checkbox">
                                <label>
                                    <input type="checkbox" name="id[]" class="chkBoxId" value="{{ $page->id }}">
                                    <span class="btn"></span>
                                </label>
                            </div>
                        </td>
                        <td>{{ $page->title }}</td>
                        <td>{{ $page->created_at->format('Y年m月d日') }}</td>
                        <td>{{ $page->is_published == '1' ? '公開中' : '非公開' }}</td>
                        <td><div class="pages__Btn"><a href="{{ route('pages.edit', $page->id) }}"><p>プレビュー</p></a></div></td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</form>
@endsection
@push('scripts')
<script>
    $('#selectAllBtn').click(function(event) {
        $(".chkBoxId").prop('checked', $(this).prop('checked'));
        chkboxCheck();
    });

    $('.chkBoxId').change(function () {
        chkboxCheck();
    });

    function chkboxCheck()
    {
        var chkBox = $('input.chkBoxId:checkbox:checked').length > 0;
        if(chkBox){
            $('#btnSubmit').attr('disabled', false);
        } else {
            $('#btnSubmit').attr('disabled', true);
        }
    }
</script>
<script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
@endpush
