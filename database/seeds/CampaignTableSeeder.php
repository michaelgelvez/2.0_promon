<?php

use App\Models\Campaign;
use Illuminate\Database\Seeder;

class CampaignTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Campaign::insert([
            'name' => null,
            'released_start_at' => null,
            'released_end_at' => null,
            'finish_redirect_url' => null,
        ]);
    }
}
