<?php

use App\Models\MasterCss;
use Illuminate\Database\Seeder;

class MasterCssTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        MasterCss::insert([
            'content' => 'body { background-color: white; }',
        ]);
    }
}
