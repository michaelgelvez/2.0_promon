<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberHeadersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('member_headers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('field');
            $table->boolean('is_encrypted');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('member_headers');
    }
}
