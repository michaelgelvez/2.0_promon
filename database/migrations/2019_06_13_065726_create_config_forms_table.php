<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigFormsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('config_forms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('form_id');
            $table->boolean('admin_is_notify');
            $table->string('admin_subject')->nullable();
            $table->text('admin_emails')->nullable();
            $table->boolean('user_is_notify');
            $table->string('user_subject')->nullable();
            $table->text('user_emails')->nullable();
            $table->longText('user_email_content')->nullable();

            //foreign keys
            $table->foreign('form_id')->references('id')->on('forms')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('config_forms');
    }
}
