<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class QuestionFormAnswers extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('question_form_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('submitted_form_id');
            $table->unsignedBigInteger('question_form_id');
            $table->longText('answer')->nullable();
            $table->string('other')->nullable();

            //foreign keys
            $table->foreign('submitted_form_id')->references('id')->on('submitted_forms');
            $table->foreign('question_form_id')->references('id')->on('question_forms')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('question_form_answers');
    }
}
