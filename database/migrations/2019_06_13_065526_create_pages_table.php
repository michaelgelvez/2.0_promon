<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('permalink', 191)->unique();
            $table->string('url');
            $table->longText('content');
            $table->string('ogp_title')->nullable();
            $table->string('ogp_type')->nullable();
            $table->string('ogp_url')->nullable();
            $table->string('ogp_image')->nullable();
            $table->string('ogp_site_name')->nullable();
            $table->string('ogp_description')->nullable();
            $table->string('social_fb_app_id')->nullable();
            $table->string('social_fb_admin_id')->nullable();
            $table->string('social_twitter_title')->nullable();
            $table->string('social_twitter_image')->nullable();
            $table->string('social_twitter_card')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_keyword')->nullable();
            $table->boolean('is_published');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
