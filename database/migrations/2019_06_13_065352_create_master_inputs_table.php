<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterInputsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('master_inputs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type');
            $table->string('name');
            $table->string('label')->nullable();
            $table->longText('options')->nullable();
            $table->boolean('is_required')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('master_inputs');
    }
}
